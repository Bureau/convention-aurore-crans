LATEXMK ?= latexmk

SOURCE = convention_crans.tex
PDF = $(SOURCE:.tex=.pdf)

.PHONY: all $(PDF) clean

all: $(PDF)

$(PDF): $(SOURCE)
	$(LATEXMK) -lualatex $(SOURCE)

clean:
	$(LATEXMK) -C $(SOURCE)
