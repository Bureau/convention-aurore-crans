% vim: ft=tex ts=2 sts=2 sw=2 et:
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{legal}[2021/04/29 Aurore legal class]

\LoadClass[12pt,a4paper]{book}

\RequirePackage{fontspec}
\RequirePackage{expl3}
\RequirePackage{xparse}
\RequirePackage[english,latin,french]{babel}
\RequirePackage[final,babel=true]{microtype}
\RequirePackage[explicit,loadonly]{titlesec}
\RequirePackage[babel,french=guillemets]{csquotes}
\RequirePackage[inline]{enumitem}
\RequirePackage{geometry}
\RequirePackage{fancyhdr}
\RequirePackage{nicefrac}
\RequirePackage{siunitx}
\RequirePackage{titletoc}
\RequirePackage{xcolor}
\RequirePackage{tikz}
\RequirePackage[pdfa]{hyperref}
\RequirePackage{bookmark}
\RequirePackage{hyperxmp}

\usetikzlibrary{
  backgrounds,
  patterns,
  shadows.blur,
  shapes.symbols,
  patterns.meta,
  fit,
}

% TODO: \pdf_object_new for .icc inclusion
% TODO: https://github.com/latex3/hyperref/issues/133 (not stable yet)

\immediate\pdfobj stream attr{/N 3} file{eciRGB_v2.icc}
\pdfcatalog{%
  /OutputIntents [ <<
    /Type /OutputIntent
    /S/GTS_PDFA1
    /DestOutputProfile \the\pdflastobj\space 0 R
    /OutputConditionIdentifier (eciRGB v2)
    /Info(eciRGB v2)
  >> ]
}


\ExplSyntaxOn

%% Draft mode

\bool_gset_false:N \g__lg_draft_bool

\DeclareOption {draft}
  {
    \bool_gset_true:N \g__lg_draft_bool
  }

\ProcessOptions\relax


%% Bookmarks

\bookmarksetup
  {
    numbered,
  }

\cs_set:Npn \Hy@numberline #1
  { #1. ~ }


%% Paper geometry

\geometry
  {
    lmargin = 42.5mm,
    rmargin = 42.4mm,
    tmargin = 37.5mm,
    bmargin = 37.5mm,
  }


%% Headers and footers

\cs_new:Nn \__lg_add_emdashes:n
  { — \enspace #1 \enspace — }

\fancypagestyle {legal}
  {
    \setlength {\headheight} { 7.5mm }

    \fancyhf {}

    \fancyfoot [C]
      { \footnotesize \textsf { \thepage } }

    \bool_if:NT \g__lg_draft_bool
      {
        \fancyhead [C]
          {
            \color { red }
            \textsf { Brouillon }
          }
      }

    \tl_set:Nn \headrulewidth { 0pt }
    \tl_set:Nn \footrulewidth { 0pt }
  }

\pagestyle {legal}


%% Paragraphs

\setlength { \parskip } { 0em }
\setlength { \parindent } { .75em }

\widowpenalty=9996
\clubpenalty=9996


%% Fonts

%\setmainfont
%  [
%    Ligatures = { Common, },
%    Numbers = { OldStyle, },
%    Ligatures = TeX,
%    Path = fonts/sabon/,
%    UprightFont = Roman.otf,
%    ItalicFont = Italic.otf,
%    BoldFont = Bold.otf,
%    BoldItalicFont = BoldItalic.otf,
%  ]
%  { Sabon LT Std }

%\setsansfont
%  [
%    Path = fonts/avenir/,
%    UprightFont = Roman.otf,
%    ItalicFont = Oblique.otf,
%    BoldFont = Heavy.otf,
%    BoldItalicFont = HeavyOblique.otf,
%    FontFace = {el} {n} { Light.otf },
%    FontFace = {el} {it} { LightOblique.otf },
%    FontFace = {l} {n} { Book.otf },
%    FontFace = {l} {it} { BookOblique.otf },
%    FontFace = {sb} {n} { Medium.otf },
%    FontFace = {sb} {it} { MediumOblique.otf },
%    FontFace = {ub} {n} { Black.otf },
%    FontFace = {ub} {it} { BlackOblique.otf },
%  ]
%  { Avenir LT Std }

\setmainfont
  [
    Ligatures = { Common },
    Numbers = OldStyle,
  ]
  { Linux Libertine O }


%% Numbers

\sisetup
  {
    detect-all,
    per-mode = repeated-symbol,
    binary-units = true,
    quotient-mode = fraction,
    fraction-function = \nicefrac,
  }

\DeclareSIUnit \euro {€}


%% Lists

\setlist [ itemize ]
  {
    parsep = \parskip,
    itemsep = 0mm,
    wide = 0mm,
    listparindent = \parindent,
    topsep = 0mm,
    labelindent = \parindent,
  }

\setlist [ enumerate ]
  {
    parsep = \parskip,
    itemsep = 0mm,
    wide = 0mm,
    listparindent = \parindent,
    topsep = 0mm,
    labelindent = \parindent,
    labelsep = 0mm,
  }

\setlist [ enumerate, 1 ]
  {
    label = \arabic*) \enspace,
  }

%% Foreign languages

\AddBabelHook {nonfrench} {foreign} {\itshape}

\DeclareExpandableDocumentCommand \latin {m}
  {
    \foreignlanguage {latin} {#1}
  }

\DeclareExpandableDocumentCommand \english {m}
  {
    \foreignlanguage {english} {#1}
  }

\DeclareExpandableDocumentCommand \ie {}
  {
    \latin {i.\,e.}
  }


%% Title

\titleclass {\lgtitle} [0] {straight}

\newcounter {lgtitle}

\tl_set:Nn \thelgtitle
  {
    \int_to_Roman:n { \value {lgtitle} }
  }

\tl_set:Nn \toclevel@lgtitle { 0 }

\cs_new:Nn \__lg_pretty_num:Nn
  {
    #1 {#2}
    \int_compare:nNnT {#2} = { 1 }
      { \ier{} }
  }

\titleformat {\lgtitle} [display]
  { \normalfont \sffamily \center }
  {
    \Large \bfseries
    Titre ~
    \__lg_pretty_num:Nn \int_to_Roman:n
      { \value {lgtitle} }
  }
  { 0mm }
  { \large #1 }

\titlespacing { \lgtitle }
  { 0mm }
  { 2.25 \baselineskip plus 1 \baselineskip minus 1 \baselineskip }
  { 2 \baselineskip plus 1 \baselineskip minus 1 \baselineskip }


%% Article

\titleclass {\lgarticle} {straight} [\lgtitle]

\newcounter {lgarticle}
\@addtoreset {lgarticle} {lgtitle}

\tl_set:Nn \thelgarticle
  {
    \thelgtitle
    \nobreak - \nobreak
    \int_to_arabic:n { \value {lgarticle} }
  }

\tl_set:Nn \toclevel@lgarticle { 1 }

\titleformat {\lgarticle} [block]
  { \sffamily \center }
  {
    \bfseries Article ~
    \__lg_pretty_num:Nn \int_to_arabic:n
      { \value {lgarticle} }.
    \enspace — \enspace
  }
  { 0pt }
  { #1. }

\titlespacing { \lgarticle }
  { 0mm }
  { 1 \baselineskip plus .25 \baselineskip minus .25 \baselineskip }
  { 1 \baselineskip plus .25 \baselineskip minus .25 \baselineskip }


%% Subdivision

\newlist {lgsubdiv} {enumerate} {1}

\setlist [lgsubdiv]
  {
    parsep = \parskip,
    itemsep = .5 \baselineskip,
    wide = 0mm,
    listparindent = \parindent,
    topsep = 0mm,
    label = \Roman*. \enspace — \enspace,
    labelsep = 0mm,
    labelindent = \parindent,
    ref = \Roman*,
  }


%% Place & subtitle

\NewDocumentCommand \place {m}
  {
    \str_gset:Nx \g__lg_place_str {#1}
  }

\NewDocumentCommand \subtitle {m}
  {
    \str_gset:Nx \g_lg_subtitle_str {#1}
  }

\AtBeginDocument
  {
    \cs_undefine:N \place
    \cs_undefine:N \subtitle
  }


%% Signatories

\seq_new:N \g__lg_sign_seq

\NewDocumentCommand \signatory {mm}
  {
    \group_begin:
    \prop_set_from_keyval:Nn \l__sign_prop
      {
        org = #1,
        name = #2,
      }
    \seq_gput_right:NV \g__lg_sign_seq
      \l__sign_prop
    \group_end:
  }

\AtBeginDocument
  {
    \cs_undefine:N \signatory
  }

\cs_new:Nn \__lg_show_signature:nn
  {
    \group_begin:
    \tl_set:Nn \l__flush_tl
      {
        \bool_if:NTF #1
          { flushright }
          { flushleft }
      }
    \begin \l__flush_tl
      \parbox { .4 \textwidth }
        {
          \centering
          \textit { Pour ~ \prop_item:Nn {#2} { org }, } \\
          \textsc { \prop_item:Nn {#2} { name } }
        }
    \end \l__flush_tl
    \group_end:
  }

% http://www.jsylvest.com/blog/2019/05/%E2%81%82-asterisms-in-latex-%E2%81%82/
\NewDocumentCommand \asterism {}
  {
    \makebox [ \baselineskip ] [c]
      {
        \makebox [ 0pt ] [c]
          {
            \raisebox { -0.6ex } { \smash { ** } }
          }
        \makebox [ 0pt ] [c]
          {
            \raisebox { 0.2ex } { \smash { * } }
          }
      }
  }

\NewDocumentCommand \makesignatures {}
  {
    \vspace* { 1 \baselineskip }
    \begin {center}
      \textsf { \asterism }
    \end {center}
    \vspace* { 1 \baselineskip }
    \par Fait ~ à ~ { \g__lg_place_str } ~ le ~ \@date.
    \par \group_begin:
    \bool_set_true:N \l__flush_right_bool
    \seq_map_inline:Nn \g__lg_sign_seq
      {
        \__lg_show_signature:nn
          { \l__flush_right_bool }
          {##1}
        \vspace* { 0.5 \baselineskip }
        \bool_set_inverse:N \l__flush_right_bool
      }
    \group_end:
  }


%% Titlepage

\definecolor { cover-blue } { HTML } { 330080 }
\definecolor { cover-red } { HTML } { a92a44 }

\pgfdeclarepattern
  {
    name = NestedSquares,
    parameters =
      {
        \pgfkeysvalueof { /pgf/pattern~keys/angle },
        \pgfkeysvalueof { /pgf/pattern~keys/line~width },
        \pgfkeysvalueof { /pgf/pattern~keys/size },
      },
    bottom~left = { \pgfpointorigin },
    top~right =
      {
        \pgfqpoint
          { \pgfkeysvalueof { /pgf/pattern~keys/size } }
          { \pgfkeysvalueof { /pgf/pattern~keys/size } }
      },
    tile~size =
      {
        \pgfqpoint
          { \pgfkeysvalueof { /pgf/pattern~keys/size } }
          { \pgfkeysvalueof { /pgf/pattern~keys/size } }
      },
    tile~transformation =
      {
        \pgftransformrotate
          { \pgfkeysvalueof { /pgf/pattern~keys/angle } }
      },
    defaults =
      {
        angle/.initial = 0,
        line~width/.initial = 0.5pt,
        size/.initial = 55mm,
      },
    code =
      {
        \pgfsetlinewidth
          { \pgfkeysvalueof { /pgf/pattern~keys/line~width } }

        \foreach \size in { 2.5, 7.5, ..., 25 }
          {
            \pgfpathrectanglecorners
              { \pgfpoint { \size mm } { \size mm } }
              {
                \pgfpoint
                  {
                    \pgfkeysvalueof { /pgf/pattern~keys/size }
                      - \size mm
                  }
                  {
                    \pgfkeysvalueof { /pgf/pattern~keys/size }
                      - \size mm
                  }
              }
          }

        \pgfusepath { stroke }
      },
  }

\DeclareDocumentCommand \maketitle {}
  {
    \vskip 1em
    \begin { center }
      { \Huge \sffamily \bfseries \@title }
      \vskip 1em
      { \huge \sffamily \g_lg_subtitle_str }
    \end { center }
    \vskip 2em
  }


%% Todo

\bool_gset_true:N \g__lg_todo_bool

\NewDocumentCommand \disabletodo {}
  {
    \bool_gset_false:N \g__lg_todo_bool
  }

\AtBeginDocument
  {
    \cs_undefine:N \disabletodo
  }

\NewDocumentCommand \todo {m}
  {
    \bool_if:NT \g__lg_todo_bool
      {
        \textit { \color {red} (#1) }
      }
  }


%% Hyperlinks

\hypersetup
  {
    unicode,
    hidelinks,
    final,
    pdfapart = 3,
    pdfaconformance = B,
    pdfproducer = {},
    pdfcreator = {},
  }

\cs_if_exist:NT \KV@Hyp@keeppdfinfo
  {
    \hypersetup
      {
        keeppdfinfo,
      }
  }


\pdfvariable suppressoptionalinfo
  \int_eval:n { 1 + 2 }

\AtBeginDocument
  {
    \seq_clear:N \l_tmpa_seq
    \seq_map_inline:Nn \g__lg_sign_seq
      {
        \seq_put_right:Nn \l_tmpa_seq
          {
            \xmpquote { \prop_item:Nn {#1} { name } }
          }
      }
    \tl_set:Nx \l_tmpa_tl
      { \seq_use:Nn \l_tmpa_seq { , ~ } }

    \hypersetup
      {
        pdftitle = { \@title {} ~ — ~ \g_lg_subtitle_str },
        pdfauthor = { \l_tmpa_tl },
      }
  }


\ExplSyntaxOff
